import { Component } from '@angular/core';
import { LoadingService } from '../../core/loading.service';

@Component({
  selector: 'app-loading-container',
  templateUrl: './loadingcontainer.component.html',
  styleUrls: ['./loadingcontainer.component.scss']
})
export class LoadingContainerComponent {
  isLoading = false;

  constructor(private readonly loadingService: LoadingService) {}

  ngOnInit() {
    this.loadingService.loadingState.subscribe((isLoading: boolean) => {
        this.isLoading = isLoading;
    });
  }
}
