<div class="academic">
    <h2>Background</h2>
    <p>
        Various studies have been done to demonstrate the relationship between video game play and the satisfaction of basal psychological needs.
        The most common reviews done for the above relation are based on self-determination theory (SDT) and as such on the needs for competence, autonomy and relatedness.
        These reviews have further shown that there is strong correlation between lack of real world need satisfaction or of increase in real world need frustration with gaming dependence.
        We assume that methods for systematically determining greater satisfaction of certain needs in the real world is a complex task and far from our reach at this time.
        As such one can only utilize our naturally imbued cognitive abilities to filter activities/methods based on the needs we know are lacking satisfaction.
    </p>

    <h2>The Needs Tree</h2>
    <p>
        Since SDT is so highly validated in the academic community, we choose to use it for our model as well (We are not the only ones to do so).
        Our model will assume that there are more fine-grained needs within each of the top-level needs and that there may be even futher nesting within our psychological needs structure.
        This automatically leads us to a tree structure for our needs model as can be seen below. We will initially only hold the top level needs but add to this tree as we validate it with data.
    </p>
    <img src="assets/needs_tree.jpg" />

    <h2>Goal Directed Behavior in Games</h2>
    <p>
        Explicit and implicit goals have been widely studied in social psychology along with their impact on human behavior.
        Our goals can help us satisfy our needs and hence they can be seen as related to specific needs.
        In video games we demonstrate goals in various ways, for example grinding (a term meaning exerting continuous monotonic effort)
        in order to acquire specific items can impact our need for competence (if we use it to defeat a boss for example) or creativity (if we use the acquired item to construct something unique).
        It is an insurmountably difficult task to show that all in-game behavior is directed by some goal, though this might be the case; it is not necessary for us to use in-game behavior to reverse engineer some goals.
        Once we know the goals that we are trying to accomplish and the needs that these goals satisfy, we can further enrich our needs profile.
    </p>

    <h3>Explicit Goals and Testing</h3>
    <p>
        Explicit goals are the objectives that we are consciously aware of and can report in self-reported assessments.
        For example if I know my goal is to reach level 25 for my character in a role-playing game, so that I can unlock a specific skill, this is a consciously acknowledged goal on my part.
        Such goals are relatively simple to test emperically, as we can use surveys and self-reported assessments to determine the specific goals for an individual.
        Unfortunately within games we most often do not proclaim our goals before we pursue them (except for quest type goals that are baked into the game itself).
        Our behavior in games is potentially in pursuit of specific goals and hence one needs to find the association between those behaviors and the underlying goals that players are trying to achieve. 
    </p>

    <h3>Implicit Goals and Testing</h3>
    <p>
        Implicit goals are the objectives that we are not consciously aware of and hence cannot report in self-reported assessments.
        For example if I am given the option within a game to go right into battle or do some reconnassance, I might choose to do reconnassance in order to reduce my risk of failure but it may also be to ensure I maintain control of the gaming environment.
        Such goals are exceptionally difficult to test as they require techniques that are able to peek into the unconscious mind. The most famous and potentially also the most controversial test designed for testing of implicit associations is the implicit associations test (IAT).
        The IAT has been critiqued by many to have low reliability as has been shown in repeat applications of the test. Unfortunately currently we do not have a highly validated method for testing implicit goals.
        Since validation of implicit goals is so difficult we will defer these for now until a time when we have better methodologies for their testing. 
    </p>

    <h3>Potential Examples Of Behaviors And Their Associated Goals</h3>
    <ul>
        <li>Ignoring the primary story quest in a adventure game and engaging in random side quests could indicate a goal to gain experience for the character or to simply explore alternative storylines/virtual geographies.</li>
        <li>Camping in a multiplayer shooter game could indicate a goal to stay alive longer than others and hence serve the need for safety/security.</li>
        <li>Pursuing a cultural or scientific victory in a game like civilization could indicate a goal to avoid conflict or risk of retaliation as opposed to pursuing a domination victory.</li>
    </ul>

    <h3>Conclusion</h3>
    <p>
        While there might be goals within games that our unconscious is trying to accomplish, there are also many goals that we can consciously acknowledge if we are asked to do so.
        As such our objective would be to determine the associations between in-game behaviors, the goals these behaviors help in pursuing and the needs that those goals satisfy.
        Since the gaming industry in generally is highly protective of their IP, few games if any have public APIs to get fine-grained behavioral/action data within games.
        We will continue to find games that systematically provide us these insights in order for us to build our model.
    </p>

    <h2>Need Satisfaction In Games</h2>
    <p>
        The games we play can provide information about the needs we are trying to satisfy. It is our assumption that there are categories within video games that satisfy particular needs better than others.
        To a great extent this assumption has already been proven to be true by exceptional individuals such as Nick Yee and their Quantic Foundry project. We also believe that specific game elements can help satisfy certain needs better than others.
        For example a rich lore can help satisfy the need for curiosity and escapism better than an empty but expansive virtual world. Our initial goal is to find the explicit associations between these elements and the needs they satisfy.
    </p>

    <h3>Examples of Game Elements</h3>
    <ul>
        <li>Genre</li>
        <li>Achievements</li>
        <li>Themes</li>
        <li>Adventure Mode: Exploratory, Linear or Both</li>
        <li>Narrative: Strong, Weak, Not Applicable</li>
        <li>Lore: Strong, Weak, Not Applicable</li>
        <li>Game Mode: Single Player, Competitive Multiplayer or Cooperative Multiplayer</li>
        <li>Competition: Low or High</li>
        <li>Complexity: Low or High</li>
        <li>Grinding: Not Applicable, Not Enough, Just About Right or Too Much</li>
        <li>Community: Supportive or Not Supportive</li>
        <li>Player Perspective: First-Person, Second-Person, Third-Person</li>
        <li>In-game Monetization: None, Tolerable, Intolerable</li>
        <li>Engagement: Easy to stop or Difficult to stop</li>
    </ul>

    <p>
        The various game elements listed above may potentially have correlations with certain needs and as such our objective would be to find if that is true.
        If it is indeed true for some of the elements with a very high confidence rate, this can further provide us insight into satisfaction of certain needs.
    </p>

    <h3>Character Archetypes</h3>
    <p>
        In role playing games and to a certain extent even in other genres, players get to play as a particular character. The character would generally have a back story, along with certain abilities.
        There are common themes in character stories and abilities, these can be illustrated as archetypes found in games. The common ones are listed below.
    </p>
    <ul>
        <li>Orphan: The neglected hero who perseveres against all odds to defeat evil.</li>
        <li>Warrior: The embodiment of physical strength, he/she can take a beating but generally is slower than other characters. While the warrior also lacks restraint, willfully jumping into a horde of enemies.</li>
        <li>Healer: The one who nurtures and supports others while preferring to stay out of the limelight.</li>
        <li>Ranger: Prefers to hunt from the relative safety of distance. Generally physically frail but agile to ensure they can run away from danger if caught in it's midst.</li>
        <li>Rogue: The silent assassin who prefers to hunt from the shadows. The Rogue is physically frail compared to the warrior but generally has high damage specially for sneak attacks and high agility.</li>
        <li>Engineer: The strategic planner who tactfully builds their victory. Intelligence and creativity is seen as the major strengths of the engineers, they are otherwise physically unimpressive.</li>
        <li>Paladin: A sorcerer that focuses on healing magic. Shares certain characteristics with the healer but is generally seen as very knowledgeable like all sorcerers.</li>
        <li>Dark Mage: A sorcerer that focuses on damaging magic. Shares certain characteristics with the rogue but is generally seen as very knowledgeable like all sorcerers.</li>
        <li>Conjurer: A sorcerer that focuses on conjuring demonic creatures. Shares certain characteristics with the the engineer but is generally seen as very knowledgeable like all sorcerers.</li>
        <li>Athlete: The energetic and competitive archetype, while there are fewer games with characters of this type, the athlete might be the archetype that best fits most competitive MMO players.</li>
    </ul>

    <p>
        Our hypothesis is that the character archetype that a player generally chooses to play as can also reveal to us their underlying psychological needs that are lacking satisfaction, though this probably will vary with personality types as well.
        This ofcourse will need to be validated with data and the underlying correlations uncovered if there are any.
    </p>

    <h2>Assessment Methodology</h2>
    <p>
        Our assessment methodology is quite simple. It consists of three different types of questions for each need/sub-need.
    </p>

    <h3>Types of questions</h3>

    <h4>Scenario</h4>
    <p>
        Based on a common scenario with choice duality encountered in games and the choices provided for that scenario, what would be the choice made by the test taker.
        Each choice would either indicate preference for that need or lack of preference for that need. Example of a question of this type for the affiliation need: <br/>
        You are playing an online multiplayer game like Elder Scrolls Online, you can play a solo game following a storyline or you could join a guild and play with other people. Do you join other people?
    </p>

    <h4>Frequency</h4>
    <p>
        This type of question will ask the gamer to recount how often they engage in a particular activity/behavior as part of their overall gaming. Example of this type of question for the affiliation need is:<br/>
        How often do you engage with other gamers in some capacity. For example communicate, compete, cooperate, form a guild, embark on an adventure together, Etcetera?
    </p>

    
    <h4>Importance</h4>
    <p>
        This type of question will ask the gamer to state how important a particular feature is for them. Example of this type of question for the affiliation need is:<br/>
        How important are guilds for a massively multiplayer online role-playing game? Guilds are like online communities where people come together to support each other and find or join other adventurers.
    </p>

    <h3>Further Considerations</h3>
    <p>
        While we will have the three different type of questions for each need, we might still end up with issues commonly encountered with self-assessments.
        Construction of the questions can be poor from the offset, specially in relation to determining association to the specific need.
        As such we will have atleast two questions for each type per need and also update our question set, based on our results if we find certain questions are acting as outliers. 
    </p>

    <h2>Summary</h2>
    <p>
        Our goal is to uncover the associations between psychological needs (initially based on SDT) and gaming choices/behaviors.
        We have the hypothesis that each of the following sources of information will show correlations with needs and hence may be utilized for purposes of evaluating unmet basal needs.
    </p>
    <ol>
        <li>The games that one chooses to play and their common elements (see game elements above).</li>
        <li>The in-game goals that one chooses to pursue through their in-game behavior.</li>
        <li>The character archetypes that one chooses to play with when they have choice between different characters.</li>
    </ol>
    <p>
        We may be incorrect in either of the above hypothesis but we will only know that with time as we validate the hypothesis with data.
    </p>

    <h2>Further Readings</h2>
    <p><a href="http://selfdeterminationtheory.org/SDT/documents/2010_PrzybylskiRigbyRyan_ROGP.pdf" target="_blank">A Motivational Model of Video Game Engagement</a></p>
    <p><a href="https://lib.dr.iastate.edu/cgi/viewcontent.cgi?article=9090&context=etd" target="_blank">Gaming as psychologically nutritious: Does need satisfaction in video games contribute to daily well-being beyond need satisfaction in the real world?</a></p>
    <p><a href="https://www.goodreads.com/book/show/39233223-working-with-video-gamers-and-games-in-therapy" target="_blank">Working with Video Gamers and Games in Therapy: A Clinician's Guide</a></p>
    <p><a href="https://www.goodreads.com/book/show/34323672-a-parent-s-guide-to-video-games" target="_blank">A Parent's Guide to Video Games by Rachel Kowert</a></p>
    <p><a href="https://www.goodreads.com/book/show/26160478-getting-gamers" target="_blank">Getting gamers by Jamie Madigan</a></p>
    <p><a href="https://www.goodreads.com/book/show/30629894-gamer-psychology-and-behavior" target="_blank">Gamer Psychology and Behavior</a></p>
</div>
