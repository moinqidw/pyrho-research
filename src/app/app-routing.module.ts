import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AcademicComponent } from './academic/academic.component';
import { ConceptComponent } from './concept/concept.component';
import { HomeComponent } from './home/home.component';
import { PrototypeComponent } from './prototype/prototype.component';

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'concept', component: ConceptComponent },
  { path: 'whitepaper', component: AcademicComponent },
  { path: 'prototype', component: PrototypeComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
