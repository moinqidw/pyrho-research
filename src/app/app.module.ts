import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatDialogModule } from '@angular/material/dialog';
import { MatDividerModule } from '@angular/material/divider';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatChipsModule } from '@angular/material/chips';
import { MatIconModule } from '@angular/material/icon';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { AcademicComponent } from './academic/academic.component';
import { ConceptComponent } from './concept/concept.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HomeComponent } from './home/home.component';
import { RegisterInterestDialogComponent } from './home/registerinterest/registerinterest.dialog';
import { ApiService } from './core/api.service';
import { HttpClientModule } from '@angular/common/http';
import { ErrorService } from './core/error.service';
import { LoadingService } from './core/loading.service';
import { LoadingContainerComponent } from './shared/loading/loadingcontainer.component';
import { ParentsComponent } from './home/parents/parents.component';
import { ExpertsComponent } from './home/experts/experts.component';
import { PrototypeComponent } from './prototype/prototype.component';
import { FlexLayoutModule } from '@angular/flex-layout';

@NgModule({
  declarations: [
    AppComponent,
    AcademicComponent,
    ConceptComponent,
    HomeComponent,
    ParentsComponent,
    ExpertsComponent,
    PrototypeComponent,
    RegisterInterestDialogComponent,
    LoadingContainerComponent
  ],
  entryComponents: [
    RegisterInterestDialogComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MatButtonModule,
    MatCardModule,
    MatDialogModule,
    MatDividerModule,
    MatFormFieldModule,
    MatInputModule,
    MatChipsModule,
    MatIconModule,
    MatTooltipModule,
    MatCheckboxModule,
    MatProgressSpinnerModule,
    FormsModule,
    FlexLayoutModule,
    ReactiveFormsModule,
    HttpClientModule,
    ServiceWorkerModule.register('ngsw-worker.js', {
      enabled: environment.production,
      // Register the ServiceWorker as soon as the app is stable
      // or after 30 seconds (whichever comes first).
      registrationStrategy: 'registerWhenStable:30000'
    }),
    BrowserAnimationsModule
  ],
  providers: [
    ApiService,
    ErrorService,
    LoadingService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
