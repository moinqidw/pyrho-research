import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

import { distinctUntilChanged } from 'rxjs/operators';


@Injectable()
export class ErrorService {
  private errorSubject = new BehaviorSubject<string | null>(null);
  public errorState = this.errorSubject.asObservable().pipe(distinctUntilChanged());

  constructor() {}

  setError(message: string) {
      this.errorSubject.next(message);
  }

  clearError() {
      this.errorSubject.next(null);
  }

  getCurrentErrorMessage(): string | null {
    return this.errorSubject.value;
  }
}
