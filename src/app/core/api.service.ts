import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';

import { environment } from '../../environments/environment';
import { ErrorService } from './error.service';

@Injectable()
export class ApiService {
    constructor(private http: HttpClient, private errorService: ErrorService) {
        this.formatErrors = this.formatErrors.bind(this);
    }

    private formatErrors(response: any) {
        this.errorService.setError(response.error.error);
        return throwError(response.error);
    }

    post(path: string, body: Object = {}): Observable<any> {
        const headers = new HttpHeaders();
        headers.set('Content-Type', 'application/json');
        headers.set('Accept', 'application/json');
        return this.http.post(`${environment.apiUrl}/${path}`, body, { headers }).pipe(catchError(this.formatErrors)).pipe(map((response: {}) => response ));
    }
}