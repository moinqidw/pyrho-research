import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

import { distinctUntilChanged } from 'rxjs/operators';


@Injectable()
export class LoadingService {
  private loadingStateSubject = new BehaviorSubject<boolean>(false);
  public loadingState = this.loadingStateSubject.asObservable().pipe(distinctUntilChanged());
  
  private successSubject = new BehaviorSubject<string | null>(null);
  public successState = this.successSubject.asObservable().pipe(distinctUntilChanged());

  constructor() {}

  startLoading() {
      this.loadingStateSubject.next(true);
  }

  endLoading() {
      this.loadingStateSubject.next(false);
  }

  getCurrentLoadingState(): boolean {
    return this.loadingStateSubject.value;
  }

  setSuccessMessage(message: string) {
    this.successSubject.next(message);
  }

  clearSuccessMessage() {
      this.successSubject.next(null);
  }

  getCurrentErrorMessage(): string | null {
    return this.successSubject.value;
  }
}
