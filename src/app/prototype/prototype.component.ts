import { Component, OnInit } from '@angular/core';

import { PSYCHOS, IPSYCHO } from './psychos';

import { ErrorService } from '../core/error.service';
import { LoadingService } from '../core/loading.service';

@Component({
  selector: 'app-prototype',
  templateUrl: './prototype.component.html',
  styleUrls: ['./prototype.component.scss']
})
export class PrototypeComponent implements OnInit {
    concern?: string;
    concernSubmitted: boolean = false;
    psychos: Array<IPSYCHO> = PSYCHOS;
    selectedPsycho?: IPSYCHO;
    followups: Array<string> = [];
    followup?: string;
  
    constructor() {}

    ngOnInit(): void {}

    submitConcern() {
        this.concernSubmitted = true;
    }

    selectPsycho(psycho: IPSYCHO) {
        this.selectedPsycho = psycho;
    }

    submitFollowup() {
        this.followups.push(this.followup!);
        this.followup = undefined;
    }
}
