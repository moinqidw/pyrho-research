export interface IPSYCHO {
    name: string,
    specialty: string,
    education: string,
    protip: string,
    image: string,
    cost: number,
    additionalCost: number,
    followups: number,
    rating: number
}

export const PSYCHOS: Array<IPSYCHO> = [
    {
        name: 'Dr. Claire Monroe',
        specialty: 'Adolescent Psychology',
        education: 'University of Ottawa',
        image: 'psycho_1.jpeg',
        cost: 9,
        rating: 4,
        additionalCost: 0.5,
        followups: 5,
        protip: 'Hiding the candy stash in the laundry room will help keep your children healthy and help you maintain your sanity.'
    },
    {
        name: 'Dr. Bryan Bell',
        specialty: 'Child Psychology',
        education: 'University of Michigan',
        image: 'psycho_2.jpeg',
        cost: 7,
        rating: 3,
        additionalCost: 0.8,
        followups: 8,
        protip: 'Buy lots of mac and cheese. That’s the key. Don’t fight it. Just buy as much as you can. It’ll be fine.'
    },
    {
        name: 'Dr. Mike Anderson',
        specialty: 'Developmental Psychology',
        education: 'UC - Irvine',
        image: 'psycho_3.jpeg',
        cost: 5,
        rating: 1,
        additionalCost: 0.8,
        followups: 6,
        protip: 'If you turn screen time into currency, you can get your children to do anything from homework to cleaning toilets.'
    },
    {
        name: 'Dr. Fatima Ansari',
        specialty: 'Educational Psychology',
        education: 'McGill University',
        image: 'psycho_4.jpeg',
        cost: 9,
        rating: 5,
        additionalCost: 0.9,
        followups: 10,
        protip: 'If you find a long lost sippy cup, don’t open it. Just throw it away. Opening it is like opening the seventh seal.'
    },
    {
        name: 'Dr. Alice Bryce',
        specialty: 'Developmental Psychology',
        education: 'Purdue University',
        image: 'psycho_5.jpeg',
        cost: 12,
        rating: 4,
        additionalCost: 2,
        followups: 8,
        protip: 'Wet wipes can clean everything from butts to counters to car seats. They are your Swiss Army knife.'
    },
    {
        name: 'Dr. Kyra Smith',
        specialty: 'Child Psychology',
        education: 'University of Washington',
        image: 'psycho_6.jpeg',
        cost: 15,
        rating: 3,
        additionalCost: 1.5,
        followups: 10,
        protip: 'Don’t worry about making sure your kid looks cute all the time. Sometimes it’s all you can do to keep their pants on.'
    },
    {
        name: 'Dr. Angela Hung',
        specialty: 'Adolescent Psychology',
        education: 'University of Colorado',
        image: 'psycho_7.jpg',
        cost: 13,
        rating: 5,
        additionalCost: 1.2,
        followups: 8,
        protip: 'The shame you feel for using a crayon instead of a pen is not nearly as painful as searching for a pen while holding a toddler.'
    },
    {
        name: 'Dr. Greg Pearce',
        specialty: 'Cognitive Psychology',
        education: 'Western University',
        image: 'psycho_8.jpeg',
        cost: 9,
        rating: 4,
        additionalCost: 1,
        followups: 6,
        protip: 'Taking your kids to the park is a really good way to get caught up on social media.'
    }
]