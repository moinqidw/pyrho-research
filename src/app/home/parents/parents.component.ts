import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ApiService } from '../../core/api.service';
import { ErrorService } from '../../core/error.service';
import { LoadingService } from '../../core/loading.service';
import { RegisterInterestDialogComponent } from '../registerinterest/registerinterest.dialog';


@Component({
    selector: 'app-parents',
    templateUrl: './parents.component.html',
    styleUrls: ['./parents.component.scss']
})
export class ParentsComponent implements OnInit {
    videoWidth: number | null = null;
    videoHeight: number | null = null;
    
    constructor(private dialog: MatDialog, private apiService: ApiService, private loader: LoadingService, private errorService: ErrorService) { }
  
    ngOnInit(): void {
      if (window.outerWidth > 800) {
        this.videoWidth = 600;
        this.videoHeight = 350;
      } else {
        this.videoWidth = 300;
        this.videoHeight = 170;
      }
    }
  
    registerInterest() {
      const dialogRef = this.dialog.open(RegisterInterestDialogComponent, { minWidth: window.outerWidth > 1024 ? 500 : 200 });
      dialogRef.afterClosed().subscribe(data => {
        if (!!data) {
          this.loader.startLoading();
          this.loader.clearSuccessMessage();
          this.errorService.clearError();
          this.apiService.post('register_interest', { email: data.email.replace(/\s/g, ""), comments: `${data.comments}\nUser Interested: ${data.interested ? true : false}` })
          .subscribe(_ => {
            this.loader.endLoading();
            this.loader.setSuccessMessage(`Thank you for registering your interest with Pyrho. We will keep you updated, please check your inbox for notifications from us.`);
          }, error => {
            this.loader.endLoading();
            this.errorService.setError(`An error occurred when trying to register interest, this generally happens with blocked/invalid email addresses. Please reach out to us at support@pyrho.net.`);
          });
        }
      });
    }
  
}
  