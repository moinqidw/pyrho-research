import { Component } from '@angular/core';

@Component({
  selector: 'app-registerinterestdialog',
  templateUrl: './registerinterest.dialog.html',
  styleUrls: ['./registerinterest.dialog.scss']
})
export class RegisterInterestDialogComponent {
  comments?: string
  email?: string
  interested: boolean = false;
}
