import { Component, OnInit } from '@angular/core';
import { ErrorService } from '../core/error.service';
import { LoadingService } from '../core/loading.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  errorMessage: string | null = null;
  successMessage: string | null = null;
  musicInfo: string = `Music: Happy Clappy Ukulele by Shane Ivers - https://www.silvermansound.com \nLicensed under Creative Commons Attribution 4.0 International License\nhttps://creativecommons.org/licenses/by/4.0/\nMusic promoted by https://www.chosic.com/ `

  constructor(private loader: LoadingService, private errorService: ErrorService) { }

  ngOnInit(): void {
    this.errorService.errorState.subscribe(errorMessage => this.errorMessage = errorMessage);
    this.loader.successState.subscribe(successMessage => this.successMessage = successMessage);
  }
}
