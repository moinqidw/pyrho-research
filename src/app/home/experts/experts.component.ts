import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ApiService } from '../../core/api.service';
import { ErrorService } from '../../core/error.service';
import { LoadingService } from '../../core/loading.service';
import { RegisterInterestDialogComponent } from '../registerinterest/registerinterest.dialog';


@Component({
    selector: 'app-experts',
    templateUrl: './experts.component.html',
    styleUrls: ['./experts.component.scss']
})
export class ExpertsComponent implements OnInit {
    videoWidth: number | null = null;
    videoHeight: number | null = null;
    email?: string;
    
    constructor(private dialog: MatDialog, private apiService: ApiService, private loader: LoadingService, private errorService: ErrorService) { }
  
    ngOnInit(): void {
      if (window.outerWidth > 800) {
        this.videoWidth = 600;
        this.videoHeight = 350;
      } else {
        this.videoWidth = 300;
        this.videoHeight = 170;
      }
    }

    registerInterest() {
      this.loader.startLoading();
      this.loader.clearSuccessMessage();
      this.errorService.clearError();
      this.apiService.post('register_interest', { email: this.email!.replace(/\s/g, ""), comments: `Interest shown for Pyrho Experts.` })
      .subscribe(_ => {
        this.loader.endLoading();
        this.loader.setSuccessMessage(`Thank you for registering your interest with Pyrho. We will keep you updated, please check your inbox for notifications from us.`);
      }, error => {
        this.loader.endLoading();
        this.errorService.setError(`An error occurred when trying to register interest, this generally happens with blocked/invalid email addresses. Please reach out to us at support@pyrho.net.`);
      });
    }
  
}
  